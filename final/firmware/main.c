#include <avr/io.h>
#include <util/delay.h>

void init_adc()
{
    ADCSRA = (ADCSRA | (1<<ADEN)); //Enable ADC
    ADCSRA = (ADCSRA | (1<<ADPS2)); //Set sampling rate
    ADMUX  = (ADMUX | (1<<REFS0)); // set reference voltage
}

unsigned int read_adc()
{
    int p=0b00000000; //select input pin
    ADMUX=ADMUX | p; // Set ADMUX = 01000000
    ADCSRA=ADCSRA | (1<<ADSC); //start conversion
    while((ADCSRA & (0b00000001<<ADIF))==0) //check whether conversion completed
    {}
    ADCSRA=ADCSRA | (1<<ADIF); //reset the conversion completion flag
    return(ADC); //return output
}

void delay(unsigned int i)
{
    int j;
    for(j=0; j<i; j++)
    {
        _delay_ms(1);
    }
}
int c[1];
int * controlSpeed()
{
    _delay_ms(500); //delay to allow ADC initialization
    unsigned int a;
    unsigned int q;
    
    unsigned int T;
    unsigned int TON;
    unsigned int TOFF;
    
    q=read_adc();
    a=q/40;
    T=2.56;
    TOFF=a;
    
    TON=T-TOFF;
    q=read_adc();
    a=q/40;
    T=2.56;
    TOFF=a;
    
    TON=T-TOFF;
    c[0]=TON;
    c[1]=TOFF;
    return c;
}

int main(){
	//ADR code for speed control
    init_adc(); //initliaze ADC
    int* local = controlSpeed();
	DDRD = 0b00000000;
	DDRB = 0b00001111;
	PORTD = 0b11111111;
	int c;
	while(1)
	{
		c = PIND;
//A=1 (LF) and B = 1(RT)
        if(c==0b11111111){
			PORTB= 0b00001010;
		}
        if(c==0b11111011){
            PORTB= 0b00010000;
            //Loop Buzz
        }
        if(c==0b11110011){
            PORTB= 0b00010000;
            //Loop Buzz
        }
        if(c==0b11110111){
            //MF with  4v
            
            PORTB= 0b00001010;
            delay(local[0]);
            PORTB= 0b00001010;
            delay(local[1]);
            
        }
//A=1 (LF) and B = 0(RT)
        if(c==0b11111101){
            PORTB= 0b00001001;
        }
        if(c==0b11111001){
            PORTB= 0b00010000;
            //Loop Buzz
        }
        if(c==0b11110001){
            PORTB= 0b00010000;
            //Loop Buzz
        }
        if(c==0b11110101){
            //TR with  4v
            PORTB= 0b00001001;
            delay(local[0]);
            PORTB= 0b00001001;
            delay(local[1]);

        }

//A=0 (LF) and B = 1(RT)
        if(c==0b11111110){
            PORTB= 0b00000110;
        }
        if(c==0b11111010){
            PORTB= 0b00010000;
            //Loop Buzz
        }
        if(c==0b11110010){
            PORTB= 0b00010000;
            //Loop Buzz
        }
        if(c==0b11110110){
            //TL with  4v
            PORTB= 0b00000110;
            delay(local[0]);
            PORTB= 0b00000110;
            delay(local[1]);

        }

//A=0 (LF) and B = 0(RT)
        if(c==0b11111100){
            PORTB= 0b00011010;
            //1 Buzz
        }
        if(c==0b11111000){
            PORTB= 0b00010000;
            //Loop Buzz
        }
        if(c==0b11110000){
            PORTB= 0b00010000;
            //Loop Buzz
        }
        if(c==0b11110100){
            //MF with  4v
            //1 Buzz
        }

        
    }
	return 0;
	
}
