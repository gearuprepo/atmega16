/* Name: main.c
 * Author: <insert your name here>
 * Copyright: <insert your copyright message here>
 * License: <insert your license reference here>
 */

#include <avr/io.h>
#include <util/delay.h>

int main(void)
{
    DDRD = 0b00000000;
    DDRB = 0b00001111;
    PORTD = 0b11111111;
    int c;
    while(1)
    {
        c = PIND;
        if(c==0b11110000){ //All Sensors are on table
            PORTB = 0b00001010; //FWD
        }
//One Sensor out
        if(c==0b11111000){
            PORTB = 0b00000101;
            _delay_ms(350);
            PORTB = 0b00001001; //TR
            _delay_ms(500);
        }
        if(c==0b11110100){
            PORTB = 0b00000101;
            _delay_ms(300);
            PORTB = 0b00000110; //TL
            _delay_ms(200);
        }
        if(c==0b11110010){
            PORTB = 0b00000101;
            _delay_ms(300);
            PORTB = 0b00001001; //TR
            _delay_ms(200);
        }
        if(c==0b11110001){
            PORTB = 0b00000101;
            _delay_ms(300);
            PORTB = 0b00000110; //TL
            _delay_ms(200);
        }
//2 Sensor Output
        if(c==0b11111100){ //1 and 2
            PORTB = 0b00000101;
            _delay_ms(300);
            PORTB = 0b00001001; //TR
            _delay_ms(200);
        }
        if(c==0b11111100){ //1 and 4
            PORTB = 0b00001001; //TR
        }
        if(c==0b11111100){ //3 and 4
            PORTB = 0b00001010; //GF
        }
        if(c==0b11111100){ //2 and 3
            PORTB = 0b00000110; //TL
        }

//3 Sensor Output
        if(c==0b11111100){ //1,2,3
            PORTB = 0b00000110; //TL
        }
        if(c==0b11111100){ //2,3,4
            PORTB = 0b00000110; //TL
        }
        if(c==0b11111100){ //1,3,4
            PORTB = 0b00001001; //TR
        }
        if(c==0b11111100){ //1,2,3
            PORTB = 0b00001001; //TR
        }

        
    }
    return 0;   /* never reached */
}
